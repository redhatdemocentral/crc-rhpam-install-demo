#!/bin/sh 

# Collection of container functions used in demos.
#

# prints the documentation for this script.
function print_docs() 
{
	echo "The default option is to run this using CodeReady Containers, an OpenShift Container"
	echo "Platform for your local machine. This host has been set by default in the variables at"
	echo "the top of this script. You can modify if needed for your own host and ports by mofifying"
	echo "these variables:"
	echo
	echo "    HOST_IP=api.crc.testing"
  echo "    HOST_PORT=6443"
	echo
	echo "It's also possible to install this project on personal CodeReady Containers installation, just point"
  echo "this installer at your installation by passing an IP address of the hosting cluster:"
	echo
	echo "   $ ./init.sh IP"
	echo
	echo "IP could look like: 192.168.99.100"
	echo
	echo "Both methodes are validated by the install scripts."
	echo
}

# check for a valid passed IP address.
function valid_ip()
{
	local  ip=$1
	local  stat=1

	if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		OIFS=$IFS
		IFS='.'
		ip=($ip)
		IFS=$OIFS
		[[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
		stat=$?
	fi

	return $stat
}

function instance_ready()
{
	# check if operator is ready to start an instance.
	local count=0
	local created=false
	until [ $count -gt $DELAY ]
	do
		echo
		oc apply -f ${SUPPORT_DIR}/kieapp-rhpam-authoring.yaml >/dev/null 2>&1

		if [ "$?" -eq "0" ]; then
			created=true
			break
		fi
		
		echo "Operator is not finished installing yet... waiting to start an instance  [ ${count}s ]"
		sleep 5
		let count=$count+5;
	done
	
	if [ $created == "false" ]; then
		echo
		echo "The business automation operator failed to install inside ${DELAY} sec, "
		echo "maybe try to increase the wait time by increasing  the value of variable"
		echo "'DELAY' located at top of this script?"
		echo
		return 1   # false
	fi
	
	return 0   # true
}

function route_ready()
{
	# variable passed is either 'rhdmcentr' or 'kieserver' for insecure route.
	local server=$1

	# check if operator is ready to add an insecure route
	local count=0
	local created=false
	until [ $count -gt $DELAY ]
	do
		echo
		oc apply -f ${SUPPORT_DIR}/add-insecure-${server}-route.yaml >/dev/null 2>&1

		if [ "$?" -eq "0" ]; then
			created=true
			break
		fi

		echo "Operator is not finished installing server '${server}' yet... waiting to add an insecure route  [ ${count}s ]"
		sleep 5
		let count=$count+5;
	done

	if [ $created == "false" ]; then
		echo
		echo "The insecure route failed to be added to business central instance inside ${DELAY} sec, "
		echo "maybe try to increase the wait time by increasing the value of variable 'DELAY' located"
		echo "at top of this script?"
		echo
		return 1   # false
	fi

	return 0   # true
}

function container_ready()
{
	# check if container is ready.
	local count=0
	local created=false
	until [ $count -gt $DELAY ]
	do
		status=$(curl -u $KIE_ADMIN_USER:$KIE_ADMIN_PWD --output /dev/null --write-out "%{http_code}" \
		--silent --head --fail "http://insecure-${OCP_APP}-rhpamcentr-${OCP_PRJ}.${HOST_APPS}/rest/spaces")

		if [ ${status} -eq "200" ]; then
			created=true
			break
		fi

		echo "Container has not started yet... waiting on container  [ ${count}s ]"
		sleep 5
		let count=$count+5;
	done

	if [ $created == "false" ]; then
		echo
		echo "The business central container failed to start inside ${DELAY} sec, "
		echo "maybe try to increase the wait time by increasing  the value of variable"
		echo "'DELAY' located at top of this script?"
		echo
		return 1   # false
	fi

	return 0   # true
}

